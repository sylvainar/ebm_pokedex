import React from 'react';
import PropTypes from 'prop-types';

import { Provider } from 'react-redux';
import store from './redux/store';
import App from "./components/App";

export default () => (
  <Provider store={store}>
          <App />
  </Provider>
);
