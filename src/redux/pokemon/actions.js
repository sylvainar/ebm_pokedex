import {fetchPokemons, fetchPokemon} from "../../repository/pokemon.repository";

export const ACTIONS = {
  SET: 'POKEMON/SET',
  SELECT: 'POKEMON/SELECT',
};


export function setPokemons(list) {
  return {
    type: ACTIONS.SET,
    list,
  }
}

export function changeSelectedPokemon(pokemon) {
  return {
    type: ACTIONS.SELECT,
    pokemon,
  }
}

export function select(pokemon) {
  return async (dispatch) => {
    const pokemonFromApi = await fetchPokemon(pokemon);
    dispatch(changeSelectedPokemon(pokemonFromApi));
  }
}

export function fetch() {
  return async (dispatch) => {
    const pokemonList = await fetchPokemons();
    dispatch(setPokemons(pokemonList.results));
  }
}