import {ACTIONS} from './actions';
import {combineReducers} from 'redux';

export default combineReducers({
  list: (state = [], action) => {
    switch (action.type) {
      case ACTIONS.SET:
        return action.list;

      default:
        return state;
    }
  },
  selected: (state = null, action) => {
    switch (action.type) {
      case ACTIONS.SELECT:
        return action.pokemon;

      default:
        return state;
    }
  }
});
