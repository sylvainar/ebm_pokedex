import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import ReduxThunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';

import pokemonReducer from './pokemon/reducer';

let reducers = combineReducers({
  pokemon: pokemonReducer,
});

const middlewares = [
  ReduxThunkMiddleware,
  createLogger({
    collapsed: true,
    diff: true,
    duration: true,
  })
];

const store = createStore(
  reducers,
  compose(
    applyMiddleware(...middlewares),
  ),
);

export default store;