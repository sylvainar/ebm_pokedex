import agent from '../services/http';

const apiUrl = 'https://pokeapi.co/api/v2';

export async function fetchPokemons() {
  const req = agent.get(`${apiUrl}/pokemon/`).query();

  try {
    const { body } = await req;
    return body;
  } catch (err) {
    console.error(err);
  }
}

export async function fetchPokemon(pokemon) {
  const req = agent.get(pokemon.url).query();

  try {
    const { body } = await req;
    return body;
  } catch (err) {
    console.error(err);
  }
}