import React from 'react';
import {connect} from 'react-redux';


class PokemonList extends React.PureComponent {
  render() {
    return this.props.pokemonSelected ?
        <div>
        <h1>{this.props.pokemonSelected.name}</h1>
        <p>Height: {this.props.pokemonSelected.height}</p>
        <p>Weight: {this.props.pokemonSelected.weight}</p>
      </div>
      : <p>Nothing selected</p>;
  }
}

export default connect((state) => {
  return {
    pokemonSelected: state.pokemon.selected,
  }
})(PokemonList);