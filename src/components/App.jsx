import React from 'react';

import PokemonList from "./PokemonList";
import PokemonSelected from "./PokemonSelected";

export default class App extends React.PureComponent {
  render() {
    return (
      <div>
        <h1>Pokédex</h1>
        <PokemonList/>
        <PokemonSelected/>
      </div>
    );
  }
}