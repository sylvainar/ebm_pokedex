import React from 'react';
import {connect} from 'react-redux';

import PokemonDisplay from './PokemonDisplay';
import {setPokemons, fetch, select} from "../redux/pokemon/actions";

class PokemonList extends React.PureComponent {
  componentWillMount() {
    this.props.dispatch(fetch())
  }

  selectPokemon = (pokemon) => {
    this.props.dispatch(select(pokemon));
  };

  render() {
    return (
      <ul>
        {this
          .props
          .pokemonList
          .map(pokemon =>
            <PokemonDisplay
              key={pokemon.name}
              onClickButton={this.selectPokemon}
              pokemon={pokemon}
            />
          )}
      </ul>
    )
  }
}

export default connect((state) => {
  return {
    pokemonList: state.pokemon.list,
  }
})(PokemonList);